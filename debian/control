Source: kuserfeedback
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Scarlett Moore <sgmoore@kde.org>, Sandro Knauß <hefee@debian.org>,
Build-Depends: bison,
               cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.51.0~),
               flex,
               libqt5charts5-dev,
               libqt5svg5-dev,
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.8.0~),
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               qttools5-dev,
               qttools5-dev-tools (>= 5.4),
               xauth <!nocheck>,
               xvfb <!nocheck>,
Build-Depends-Indep: qdoc-qt5
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/libraries/kuserfeedback
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/kuserfeedback
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/kuserfeedback.git

Package: libkuserfeedback-l10n
Section: localization
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Breaks: libkuserfeedbackcore1 (<< 1.3.0-2~),
Replaces: libkuserfeedbackcore1 (<< 1.3.0-2~),
Description: user feedback for applications - localization files
 Framework for collecting user feedback for applications via telemetry
 and surveys.
 .
 Telemetry
  * Extensible set of data sources for telemetry.
  * Full control for the user on what data to contribute.
 Surveys
  * Distribute surveys and offer users to participate in them.
  * Survey targeting based on telemetry data.
  * Allow the user to configure how often they want to participate in surveys.
 .
 This package contains the shared localizations for KUserFeedback.

Package: kuserfeedback-dev
Section: libdevel
Architecture: any
Depends: libkuserfeedbackcore1 (= ${binary:Version}),
         libkuserfeedbackwidgets1 (= ${binary:Version}),
         qtbase5-dev,
         ${misc:Depends},
Description: development files for KUserFeedback - Qt 5 version
 Framework for collecting user feedback for applications via telemetry
 and surveys.
 .
 Telemetry
  * Extensible set of data sources for telemetry.
  * Full control for the user on what data to contribute.
 Surveys
  * Distribute surveys and offer users to participate in them.
  * Survey targeting based on telemetry data.
  * Allow the user to configure how often they want to participate in surveys.
 .
 This package contains the development header files for the Qt 5 version of
 KUserFeedback.

Package: libkuserfeedbackcore1
Architecture: any
Multi-Arch: same
Depends: libkuserfeedback-l10n (>= ${source:Version}),
         ${misc:Depends}, ${shlibs:Depends}
Description: user feedback for applications - Qt 5 core library
 Framework for collecting user feedback for applications via telemetry
 and surveys.
 .
 Telemetry
  * Extensible set of data sources for telemetry.
  * Full control for the user on what data to contribute.
 Surveys
  * Distribute surveys and offer users to participate in them.
  * Survey targeting based on telemetry data.
  * Allow the user to configure how often they want to participate in surveys.
 .
 This package contains the core library for the Qt 5 version of KUserFeedback.

Package: libkuserfeedbackwidgets1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: qml-module-org-kde-userfeedback (= ${binary:Version})
Description: user feedback for applications - Qt 5 widgets library
 Framework for collecting user feedback for applications via telemetry
 and surveys.
 .
 Telemetry
  * Extensible set of data sources for telemetry.
  * Full control for the user on what data to contribute.
 Surveys
  * Distribute surveys and offer users to participate in them.
  * Survey targeting based on telemetry data.
  * Allow the user to configure how often they want to participate in surveys.
 .
 This package contains the widgets library used by the Qt 5 version of
 KUserFeedback.

Package: qml-module-org-kde-userfeedback
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: user feedback for applications - Qt 5 QML module
 Framework for collecting user feedback for applications via telemetry
 and surveys.
 .
 Telemetry
  * Extensible set of data sources for telemetry.
  * Full control for the user on what data to contribute.
 Surveys
  * Distribute surveys and offer users to participate in them.
  * Survey targeting based on telemetry data.
  * Allow the user to configure how often they want to participate in surveys.
 .
 This package contains the QML module for the Qt 5 version of KUserFeedback.
