kuserfeedback (1.3.0-9) unstable; urgency=medium

  * Team upload.
  * CI: update/simplify configuration.
  * Backport upstream commit dc507c18a2c6c857b1202e20fa91729d724e270e to make
    it possible to disable testOpenGLInfoSource using an environment variable;
    patch upstream_tests-allow-to-disable-testOpenGLInfoSource-using-en.patch.
  * Export KUSERFEEBACK_SKIP_TESTOPENGLINFOSOURCE=1 in rules to skip the
    testOpenGLInfoSource test, as creating an OpenGL context is not reliable
    on buildds.
  * Run the tests at build time again, now that testOpenGLInfoSource is skipped.
  * Bump Standards-Version to 4.7.0, no changes required.
  * Update symbols files for GCC 14.
  * Since certain components are no more shipped, simplify the build a bit:
    - drop the different cmake invocations for -arch and -indep builds, and
      unconditionally disable the documentation
    - pass -DENABLE_CONSOLE=OFF to stop building UserFeedbackConsole altogether
    - pass -DENABLE_CLI=OFF to stop building userfeedbackctl altogether
    - drop almost all the content in debian/not-installed
  * Drop install files of packages that are no more shipped.

 -- Pino Toscano <pino@debian.org>  Fri, 23 Aug 2024 18:12:02 +0200

kuserfeedback (1.3.0-8) unstable; urgency=medium

  * Team upload.

  [ Patrick Franz ]
  * Upload to unstable for real this time.

 -- Patrick Franz <deltaone@debian.org>  Thu, 18 Jul 2024 19:04:27 +0200

kuserfeedback (1.3.0-7) experimental; urgency=medium

  * Team upload.

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Thu, 18 Jul 2024 18:38:43 +0200

kuserfeedback (1.3.0-6) experimental; urgency=medium

  [ Patrick Franz ]
  * Team upload.
  * Rebuild with tests disabled.

 -- Patrick Franz <deltaone@debian.org>  Tue, 18 Jun 2024 21:22:21 +0200

kuserfeedback (1.3.0-5) unstable; urgency=medium

  [ Patrick Franz ]
  * Team upload.
  * Disable tests.

 -- Patrick Franz <deltaone@debian.org>  Tue, 18 Jun 2024 08:57:35 +0200

kuserfeedback (1.3.0-4) experimental; urgency=medium

  * Revert "build also qt6 libraries", the qt6 binaries are now provided by
    the kf6-kuserfeedback source package.
  * Drop -bin and -doc packages that are now provided by the kf6 source
    package.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 16 Apr 2024 23:17:46 +0200

kuserfeedback (1.3.0-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Pino Toscano <pino@debian.org>  Wed, 08 Nov 2023 07:05:26 +0100

kuserfeedback (1.3.0-2) experimental; urgency=medium

  * Team upload.
  * Split the localization files from libkuserfeedbackcore1 to a new
    libkuserfeedback-l10n, as they will be useful also for the Qt 6 version
    - add proper breaks/replaces
  * Build the Qt 6 libraries:
    - add the qt6-base-dev, qt6-declarative-dev, and qt6-tools-dev build
      dependencies
    - do not build the console and the command line utilities, as they conflict
      with the Qt 5 versions
    - add new configure, build, and test steps for a separate Qt 6 variant
    - add new qt6-kuserfeedback-dev, libkuserfeedbackcoreqt6-1,
      libkuserfeedbackwidgetsqt6-1, and qml6-module-org-kde-userfeedback
      binary packages as counterparts of their Qt 5 versions
  * Tweak the descriptions of all the Qt 5 packages to mention the Qt version.

 -- Pino Toscano <pino@debian.org>  Sat, 04 Nov 2023 18:16:52 +0100

kuserfeedback (1.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update standards version to 4.6.2, no changes needed.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.16
  * Small copyright update.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Fri, 03 Nov 2023 08:24:58 +0100

kuserfeedback (1.2.0-2) unstable; urgency=medium

  * Team upload.
  * Bump the reference version used to cleanup the old
    /etc/xdg/org_kde_UserFeedback.categories to the same version of this
    changelog entry, as versions in the past do not work when upgrading.
  * Mark the xauth, and xvfb build dependencies as !nocheck, as they are needed
    only during dh_auto_test.

 -- Pino Toscano <pino@debian.org>  Wed, 23 Feb 2022 07:47:33 +0100

kuserfeedback (1.2.0-1) unstable; urgency=medium

  * Update upstream signing key for Jonathan Ridell.
  * New upstream release (1.2.0).
  * Added myself to the uploaders.
  * Update the list of installed files, gracefully handle move of
    org_kde_UserFeedback.categories from /etc to /usr.
  * Update symbols from build for 1.2.0.
  * Refresh copyright information.
  * Bump Standards-Version to 4.6.0, no changes required.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 06 Feb 2022 23:45:13 +0100

kuserfeedback (1.0.0-3) unstable; urgency=medium

  * Team upload.
  * Build the documentation only in -indep builds:
    - pass -DENABLE_DOCS=OFF to cmake in dh_auto_configure of -arch builds
    - pass -DENABLE_DOCS=ON to cmake in dh_auto_configure of -indep builds
    - move qdoc-qt5 as Build-Depends-Indep
    - drop the -DBUILD_QCH=ON cmake parameter, as it does not exist
  * Remove the unused doxygen build dependency.
  * Remove the unused debian/meta/ stuff.

 -- Pino Toscano <pino@debian.org>  Tue, 27 Oct 2020 21:38:10 +0100

kuserfeedback (1.0.0-2) unstable; urgency=medium

  [ Sandro Knauß ]
  * Add qdoc-qt5 to B-D needed for archs without qtdoc-qt5 dependency
    inside qttools5-dev-tools.

 -- Sandro Knauß <hefee@debian.org>  Thu, 22 Oct 2020 11:02:51 +0200

kuserfeedback (1.0.0-1) unstable; urgency=medium

  [ Scarlett Moore ]
  * Remove libkuserfeedbackcore1.maintscript as this package
    is NEW and will not have dangling maint scripts.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper build dependency to debhelper-compat 13
    - remove debian/compat
  * Bump Standards-Version to 4.5.0, no changes required.
  * Add Salsa CI config with team builder enabled.
  * Update copyright file.
    - Add missing copyright.
    - Add Upstream-name, Upstream-Contact fields.
    - Fix Source to point to invent.kde.org repo.
  * Update Rules file.
    - Fix dh call to use symbols helper and kf5 buildsystem.
    - Remove defunkt qtselect export. Qt4 is no more.
    - Add hardening options to build maint export.
  * Update Maintainer to be debian-qt-kde team.
  * Add myself to uploaders.
  * Fix the control file entries.
    - kuserfeedback-bin: fix Multi-Arch to any, it is binaries.
      Remove section: doc as it is not docs. Fix multi-Arch to same.
      Improve the long description.
    - libkuserfeebackcore1: remove breaks/replace ( neon merge. ).
      Improve long description.
    - libkuserfeebackwidgets: remove breaks/replace ( neon merge. ).
      Improve long description.
    - qml-module-org-kde-userfeedback:
      Improve long description.
    - kuserfeedback-doc:
      Improve long description.
    - kuserfeedback-dev:
      Improve long descriptions.
  * Add VSC git entries.
  * Update homepage links to invent.

  [ Sandro Knauß ]
  * Add Rules-Requires-Root field to control.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Enable running autotests.
  * Update copyright file.
  * kuserfeedback-bin is not Multi-Arch same compatible.
  * Fill upstream metadata.
  * Add symbols files.
  * Add Build-Depends-Package to symbols file.
  * Initial Debian release. (Closes: #970433)
  * Add myself to Uploaders.

 -- Sandro Knauß <hefee@debian.org>  Wed, 16 Sep 2020 12:53:13 +0200

kuserfeedback (1.0.0-0) bionic; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 11 Aug 2019 18:49:14 +0000
